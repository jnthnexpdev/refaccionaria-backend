const express = require('express');
const modelo_empleado = require('../../models/personal/model_nuevo_personal');

let app = express();

app.post('/empleado/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaPersonal = modelo_empleado({
        nombreEmpleado : body.nombreEmpleado,
        edadEmpleado : body.edadEmpleado,
        puestoEmpleado : body.puestoEmpleado,
        telefonoEmpleado : body.telefonoEmpleado,
        imagenEmpleado: body.imagenEmpleado,
        statusEmpleado : body.statusEmpleado
    });

    newSchemaPersonal
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok : true,
                message : "Datos empleado guardados ",
                data
            });
        }
    )

    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok : false,
                message : "Los datos no se han guardado",
                err
            })
        } 
    )
});

app.get('/obtener/empleados/', async (req, res) => {
    const empleados = await modelo_empleado.find();

    res.status(200).json({
        ok : true,
        empleados
    });
});

app.delete('/eliminar/empleado/:id', async(req, res) => {
    let id = req.params.id;
    const eliminar = await modelo_empleado.findByIdAndDelete(id);

    res.status(200).json({
        ok : true,
        msj : 'Empleado eliminado correctamente.',
    });
});

app.put('/editar/empleado/:id', async (req, res) =>{
    let id = req.params.id;
    const n_Empleado = req.body;

    const cambiar_nombre = await modelo_empleado.findByIdAndUpdate(id, n_Empleado, {new : true});

    res.status(200).json({
        ok : true,
        msj : "Nombre de producto actualizado con exito",
        cambiar_nombre
    });
});

module.exports = app;