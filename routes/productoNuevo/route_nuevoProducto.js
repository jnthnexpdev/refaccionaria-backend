const express = require('express');
const modelo_producto = require('../../models/refaccionaria/model_nuevo_producto');

let app = express();

app.post('/producto/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProducto = modelo_producto({
        nombreProducto : body.nombreProducto,
        marcaProducto : body.marcaProducto,
        presentacionProducto : body.presentacionProducto,
        contenidoProducto : body.contenidoProducto,
        costoProducto : body.costoProducto,
        proveedorProducto : body.proveedorProducto,
        cantidadIngresa : body.cantidadIngresa,
        statusProducto : body.statusProducto,
        descripcionProducto : body.descripcionProducto,
        imagenProducto : body.imagenProducto,
    });

    newSchemaProducto
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok : true,
                message : "Datos producto guardados ",
                data
            });
        }
    )

    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok : false,
                message : "Los datos no se han guardado",
                err
            })
        } 
    )
});

app.get('/obtener/productos/', async (req, res) => {
    const productos = await modelo_producto.find();

    res.status(200).json({
        ok : true,
        productos
    });
});

app.delete('/eliminar/producto/:id', async(req, res) => {
    let id = req.params.id;
    const eliminar = await modelo_producto.findByIdAndDelete(id);

    res.status(200).json({
        ok : true,
        msj : 'Producto eliminado correctamente.',
    });
});

app.put('/editar/producto/:id', async (req, res) =>{
    let id = req.params.id;
    const nombreProducto = req.body;

    const cambiar_nombre = await modelo_producto.findByIdAndUpdate(id, nombreProducto, {new : true});

    res.status(200).json({
        ok : true,
        msj : "Nombre de producto actualizado con exito",
        cambiar_nombre
    });
});



module.exports = app;