const express = require('express');
const modelo_sucursal = require('../../models/sucursales/model_nueva_sucursal');

let app = express();

app.post('/sucursal/nueva', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaSucursal = modelo_sucursal({
        nombreSucursal : body.nombreSucursal,
        direccionSucursal : body.direccionSucursal,
        telefonoSucursal : body.telefonoSucursal,
        horarioSucursal : body.horarioSucursal,
        estatusSucursal : body.estatusSucursal
    });

    newSchemaSucursal
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok : true,
                message : "Datos sucursal guardados ",
                data
            });
        }
    )

    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok : false,
                message : "Los datos no se han guardado",
                err
            })
        } 
    )
});

app.get('/obtener/sucursal/', async (req, res) => {
    const sucursal = await modelo_sucursal.find();

    res.status(200).json({
        ok : true,
        sucursal
    });
});

app.delete('/eliminar/sucursal/:id', async(req, res) => {
    let id = req.params.id;
    const eliminar = await modelo_sucursal.findByIdAndDelete(id);

    res.status(200).json({
        ok : true,
        msj : 'Sucursal eliminada correctamente.',
    });
});

app.put('/editar/sucursal/:id', async (req, res) =>{
    let id = req.params.id;
    const n_sucursal = req.body;

    const cambiar_nombre = await modelo_sucursal.findByIdAndUpdate(id, n_sucursal, {new : true});

    res.status(200).json({
        ok : true,
        msj : "Nombre de sucursal actualizado con exito",
        cambiar_nombre
    });
});

module.exports = app;