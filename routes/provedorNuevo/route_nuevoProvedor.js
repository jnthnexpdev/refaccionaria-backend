const express = require('express');
const modelo_provedor = require('../../models/provedores/model_nuevo_provedor');

let app = express();

app.post('/provedor/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProvedor = modelo_provedor({
        nombreProvedor : body.nombreProvedor,
        telefonoProvedor : body.telefonoProvedor,
        correoProvedor : body.correoProvedor,
        direccionProvedor : body.direccionProvedor,
        statusProvedor : body.statusProvedor
    });

    newSchemaProvedor
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok : true,
                message : "Datos provedor guardados ",
                data
            });
        }
    )

    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok : false,
                message : "Los datos no se han guardado",
                err
            })
        } 
    )
});

app.get('/obtener/provedores/', async (req, res) => {
    const provedores = await modelo_provedor.find();

    res.status(200).json({
        ok : true,
        provedores
    });
});

app.delete('/eliminar/provedores/:id', async(req, res) => {
    let id = req.params.id;
    const eliminar = await modelo_provedor.findByIdAndDelete(id);

    res.status(200).json({
        ok : true,
        msj : 'Provedor eliminado correctamente.',
    });
});

app.put('/editar/provedor/:id', async (req, res) =>{
    let id = req.params.id;
    const nProvedor = req.body;

    const cambiar_nombre = await modelo_provedor.findByIdAndUpdate(id, nProvedor, {new : true});

    res.status(200).json({
        ok : true,
        msj : "Nombre de producto actualizado con exito",
        cambiar_nombre
    });
});

module.exports = app;