const express = require('express');
const app = express();

app.use(require('./test'));
app.use(require('./productoNuevo/route_nuevoProducto'));
app.use(require('./personalNuevo/route_nuevoPersonal'));
app.use(require('./provedorNuevo/route_nuevoProvedor'));
app.use(require('./sucursalNueva/route_nuevaSucursal'));

module.exports = app;