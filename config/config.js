const mongoose = require('mongoose');
const app = require('../server/server');
const port = 3900;

//Se genera una promesa global 
mongoose.Promise = global.Promise;

//Conexion con mongodb
mongoose.connect('mongodb://localhost:27017/refaccionaria' , {useNewUrlParser : true})
.then(() => {
    console.log("\n - - - - - Base de datos conectada - - - - -\n");

    //Escuchar el puerto del servidor
    app.listen(port, () => {
        console.log(`\nServidor en el puerto: ${port}\n`);
    });
});