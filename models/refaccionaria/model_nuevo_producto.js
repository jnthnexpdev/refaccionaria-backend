const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let nuevo_producto_refaccionaria = new Schema ({
    nombreProducto : {type : String},
    marcaProducto : {type : String},
    presentacionProducto : {type : String},
    contenidoProducto : {type : String},
    costoProducto : {type : Number},
    proveedorProducto : {type : String},
    cantidadIngresa : {type : Number},
    statusProducto : {type: String},
    descripcionProducto : {type : String},
    imagenProducto : {type : String}
});

module.exports = mongoose.model('nuevoProducto', nuevo_producto_refaccionaria);