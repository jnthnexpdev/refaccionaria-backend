const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevo_provedor = new Schema({
    nombreProvedor : {type : String},
    telefonoProvedor : {type : Number},
    correoProvedor : {type : String},
    direccionProvedor : {type : String},
    statusProvedor : {type : String}
});

module.exports = mongoose.model('nuevoProvedor', nuevo_provedor);