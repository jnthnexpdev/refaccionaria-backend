const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevo_empleado = new Schema({
    nombreEmpleado : {type : String},
    edadEmpleado : {type : Number},
    puestoEmpleado : {type : String},
    telefonoEmpleado : {type : Number},
    imagenEmpleado : {type : String},
    statusEmpleado : {type : String}
});

module.exports = mongoose.model('nuevoEmpleado', nuevo_empleado);