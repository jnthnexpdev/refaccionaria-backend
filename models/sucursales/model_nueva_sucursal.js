const mongoose = require('mongoose');
let Schema  = mongoose.Schema;

let nueva_sucursal = new Schema({
    nombreSucursal : {type : String},
    direccionSucursal : {type : String},
    telefonoSucursal : {type : String},
    horarioSucursal : {type : String},
    estatusSucursal : {type : String}
});

module.exports = mongoose.model('nuevaSucursal', nueva_sucursal);