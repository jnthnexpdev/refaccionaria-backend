//Llamar librerias de body parser y express
const bodyParser = require('body-parser');
const express = require('express');

//Iniciar express
const app = express();

//Activar cors
app.use(function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Header", "Origin , X-Requested-Width, Content-Type, Accept");
    res.header("Acces-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATHC, DELETE");
    next();
}); 

//Activar middlewares de express
app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json());

app.use(require('../routes/routes'));

module.exports = app;